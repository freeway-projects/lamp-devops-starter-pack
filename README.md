# LAMP DevOps starter pack

This is an starter pack of scripts to carry out DevOps tasks.  The scripts will enable websites to be created, developed, tested and deployed.  This starter pack is designed to be used as a starting point for DevOps projects - it should then be customised and extended as required.